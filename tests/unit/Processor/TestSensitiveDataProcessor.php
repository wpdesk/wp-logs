<?php

namespace unit\Processor;

use WP_Mock;
use WP_Mock\Tools\TestCase;
use WPDesk\Logger\Processor\SensitiveDataProcessor;

class TestSensitiveDataProcessor extends TestCase
{

	public function setUp(): void {
		WP_Mock::setUp();
	}

	public function tearDown(): void {
		WP_Mock::tearDown();
	}

	public function testShouldReplaceWhenMatched() {
		$processor = new SensitiveDataProcessor( array( 'password' => '***' ) );
		$record = array(
			'password' => 'password',
			'array' => array(
				'password' => 'password',
			),
		);
		$expected = array(
			'password' => '***',
			'array' => array(
				'password' => '***',
			),
		);
		$this->assertEquals( $expected, $processor->__invoke( $record ) );
	}

	public function testShouldNotReplaceWhenNotMatched() {
		$processor = new SensitiveDataProcessor( array( 'password' => '***' ) );
		$record = array(
			'text' => 'text',
			'array' => array(
				'text' => 'text',
			),
		);
		$expected = array(
			'text' => 'text',
			'array' => array(
				'text' => 'text',
			),
		);
		$this->assertEquals( $expected, $processor->__invoke( $record ) );
	}

}
