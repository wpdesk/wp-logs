<?php

use Monolog\Handler\AbstractHandler;
use WPDesk\Logger\LoggerFacade;
use WPDesk\Logger\WPDeskLoggerFactory;

class TestLoggerFacade extends WP_UnitTestCase {
    public function tearDown()
    {
        parent::tearDown();
        WPDeskLoggerFactory::tearDown();
    }

    public function testCanCreateLogger() {
        $logger = LoggerFacade::getLogger();
        $this->assertSame($logger, LoggerFacade::get_logger(), "Should return same logger instance");
    }

    /**
     * @return void
     */
    private function expectThatLoggerGetsHit() {
        $logger = LoggerFacade::getLogger();

        $listenHandle = $this->createMock(AbstractHandler::class);

        $listenHandle
            ->expects($this->atLeastOnce())
            ->method('handle')
            ->willReturn(true);

        $listenHandle
            ->expects($this->atLeastOnce())
            ->method('isHandling')
            ->willReturn(true);

        $logger->pushHandler($listenHandle);
    }

    public function testWPError() {
        $this->expectThatLoggerGetsHit();
        LoggerFacade::log_wp_error(new \WP_Error(), debug_backtrace());
    }

    public function testLogException() {
        $this->expectThatLoggerGetsHit();
        LoggerFacade::log_exception(new RuntimeException());
    }

    public function testLogMessageBacktrace() {
        $this->expectThatLoggerGetsHit();
        LoggerFacade::log_message_backtrace('whatever', debug_backtrace());
    }

    public function testLogMessage() {
        $this->expectThatLoggerGetsHit();
        LoggerFacade::log_message('whatever');
    }


}

