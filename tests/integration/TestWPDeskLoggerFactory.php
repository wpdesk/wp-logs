<?php

use Monolog\Handler\AbstractHandler;
use Monolog\Logger;
use Monolog\Registry;
use WPDesk\Logger\WC\WooCommerceCapture;
use WPDesk\Logger\WC\WooCommerceMonologPlugin;
use WPDesk\Logger\WPDeskLoggerFactory;

class TestWPDeskLoggerFactory extends WP_UnitTestCase
{
    const LOGGER_NAME = 'somerandomname';

    public function tearDown()
    {
        parent::tearDown();
        WPDeskLoggerFactory::tearDown(self::LOGGER_NAME);
    }

    public function testCanCreateLogger()
    {
        $factory = new WPDeskLoggerFactory();
        $this->assertInstanceOf(Logger::class, $factory->createWPDeskLogger(self::LOGGER_NAME), "Logger should be created");
        $this->assertTrue($factory->isLogWorking(self::LOGGER_NAME));
    }

    public function testWCIsNotCorruptedByOtherTests()
    {
        $this->assertInstanceOf(WC_Logger::class, wc_get_logger(), "Logger should be NOT captured by default.");
    }

    public function testWCLoggingIsCapturedByOurLogs()
    {
        if (WooCommerceCapture::isSupportedWCVersion()) {
            (new WPDeskLoggerFactory())->createWPDeskLogger();
            $this->assertInstanceOf(WooCommerceMonologPlugin::class, wc_get_logger(), "Logger should be captured.");
        } else {
            $this->markTestSkipped("Cant capture logger in WC < 3.5");
        }
    }
//
//   This test won't work in PHPUnit 5.x. Enable when support for PHP 5.6 is dropped
//
//    public function testOurLogsGetAllErrors()
//    {
//        $someMessage = 'whatever';
//        $errorType = E_USER_NOTICE;
//
//        $logger = (new WPDeskLoggerFactory())->createWPDeskLogger(self::LOGGER_NAME);
//        $logger->pushHandler($this->prepareListenHandleThatIsWaitingForMessage('E_USER_NOTICE: ' . $someMessage));
//        $this->expectException(Notice::class);
//        trigger_error($someMessage, $errorType);
//    }

    /**
     * Prepares listener that check if logger gets sent message
     *
     * @param $message
     * @return AbstractHandler
     */
    private function prepareListenHandleThatIsWaitingForMessage($message)
    {
        $listenHandle = $this->createMock(AbstractHandler::class);

        $listenHandle
            ->expects($this->atLeastOnce())
            ->method('handle')
            ->with($this->callback(function ($record) use ($message) {
                $this->assertEquals($message, $record['message'], "Monolog should get message sent to logger");
                return $record['message'] === $message;
            }))
            ->willReturn(true);

        $listenHandle
            ->expects($this->atLeastOnce())
            ->method('isHandling')
            ->willReturn(true);

        /** @var $listenHandle AbstractHandler */
        return $listenHandle;
    }

    public function testOurLogsGetAllMessagesLoggedToWC()
    {
        Registry::clear();
        if (WooCommerceCapture::isSupportedWCVersion()) {
            $someMessage = 'whatever';
            $logger = (new WPDeskLoggerFactory())->createWPDeskLogger();
            $logger->pushHandler($this->prepareListenHandleThatIsWaitingForMessage($someMessage));

            wc_get_logger()->debug($someMessage);
        } else {
            $this->markTestSkipped("Cant capture logger in WC < 3.5");
        }
    }

    public function testLoggerWorksAndCanLogInGeneral()
    {
        $someMessage = 'whatever';
        $logger = (new WPDeskLoggerFactory())->createWPDeskLogger(self::LOGGER_NAME);
        $logger->pushHandler($this->prepareListenHandleThatIsWaitingForMessage($someMessage));

        $logger->debug($someMessage);
    }

    public function testAllLoggedMessagesAreWrittenToWPDeskFile()
    {
        $someMessage = 'whatever';

        $factory = new WPDeskLoggerFactory();
        $logFilename = $factory->getFileName(self::LOGGER_NAME);

        @unlink($logFilename);
        $this->assertFileNotExists($logFilename);

        $logger = $factory->createWPDeskLogger(self::LOGGER_NAME);
        $logger->debug($someMessage);
        $this->assertFileExists($logFilename);
    }

    public function testAllLoggedMessagesAreWrittenToWC()
    {
        Registry::clear();
        if (WooCommerceCapture::isSupportedWCVersion()) {
            $mockWcLogger = $this->createMock(WC_Logger::class);
            $mockWcLogger
                ->expects($this->atLeastOnce())
                ->method('log')
                ->willReturn(true);

            add_filter('woocommerce_logging_class', function () use ($mockWcLogger) {
                return $mockWcLogger;
            }, 0, 100);

            $someMessage = 'whatever';
            $logger = (new WPDeskLoggerFactory())->createWPDeskLogger();

            $logger->debug($someMessage);
        } else {
            $this->markTestSkipped("Cant capture logger in WC < 3.5 so we cant mock for this test");
        }

    }

    public function testDisable() {
        $factory = new WPDeskLoggerFactory();
        $logger = $factory->createWPDeskLogger(self::LOGGER_NAME);
        $factory->disableLog(self::LOGGER_NAME);

        $listenHandle = $this->createMock(AbstractHandler::class);

        $listenHandle
            ->expects($this->never())
            ->method('handle');

        $logger->pushHandler($listenHandle);

        $logger->debug('whatever');
    }
}

