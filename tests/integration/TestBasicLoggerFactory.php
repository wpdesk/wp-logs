<?php

use Monolog\Logger;
use WPDesk\Logger\BasicLoggerFactory;

class TestBasicLoggerFactory extends WP_UnitTestCase {

    const LOGGER_NAME = 'test-channel';

    public function testCanCreateLogger() {
        $factory = new BasicLoggerFactory();
        $logger = $factory->createLogger(self::LOGGER_NAME);
        $this->assertInstanceOf(Logger::class, $logger, "Logger should be created");
    }

    public function testCanReturnLoggerFromRegistry() {
        $factory = new BasicLoggerFactory();
        $logger1 = $factory->createLogger(self::LOGGER_NAME);
        $logger2 = $factory->createLogger(self::LOGGER_NAME);
        $logger3 = $factory->getLogger();

        $this->assertSame($logger1, $logger2, "Should return the same loggers");
        $this->assertSame($logger2, $logger3, "Should return the same loggers");

        $otherLogger = $factory->createLogger('other-name');
        $this->assertNotSame($logger1, $otherLogger, "Should return different loggers");
    }
}

