<?php

use Monolog\Logger;
use WPDesk\Logger\WC\Exception\WCLoggerAlreadyCaptured;
use WPDesk\Logger\WC\WooCommerceCapture;
use WPDesk\Logger\WC\WooCommerceMonologPlugin;

class TestWooCommerceCapture extends WP_UnitTestCase
{
    public function testIfCanCaptureWcLogger()
    {
        if (WooCommerceCapture::isSupportedWCVersion()) {
            $wcCapture = new WooCommerceCapture($this->createMock(Logger::class));
            $wcCapture->captureWcLogger();

            $this->assertInstanceOf(WooCommerceMonologPlugin::class, wc_get_logger(), "Logger should be captured.");
        } else {
            $this->markTestSkipped("Cant capture logger in WC < 3.5");
        }
    }

    public function testIfCanFreeWcLogger()
    {
        if (WooCommerceCapture::isSupportedWCVersion()) {
            $wcCapture = new WooCommerceCapture($this->createMock(Logger::class));
            $wcCapture->captureWcLogger();
            $wcCapture->freeWcLogger();
            $this->assertNotInstanceOf(WooCommerceMonologPlugin::class, wc_get_logger(),
                "Logger should be restored to original");

            $wcCapture->captureWcLogger();
            $this->assertInstanceOf(WooCommerceMonologPlugin::class, wc_get_logger(), "Logger should be captured.");
            $wcCapture->freeWcLogger();
            $this->assertNotInstanceOf(WooCommerceMonologPlugin::class, wc_get_logger(),
                "Logger should be restored to original - twice");
        } else {
            $this->markTestSkipped("Cant capture logger in WC < 3.5");
        }
    }

    public function testIfCantCaptureTwice()
    {
        if (WooCommerceCapture::isSupportedWCVersion()) {
            $this->expectException(WCLoggerAlreadyCaptured::class);
            $wcCapture = new WooCommerceCapture($this->createMock(Logger::class));
            $wcCapture->captureWcLogger();
            $wcCapture->captureWcLogger();
        } else {
            $this->markTestSkipped("Cant capture logger in WC < 3.5");
        }
    }

}

