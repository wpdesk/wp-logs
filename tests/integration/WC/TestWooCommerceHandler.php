<?php

use Monolog\Logger;
use WPDesk\Logger\WC\WooCommerceHandler;

class TestWooCommerceHandler extends WP_UnitTestCase
{

    public function testCanWriteToLogger()
    {
        $logRecord = [
            'level' => Logger::DEBUG,
            'message' => 'some message',
            'context' => [],
            'extra' => []
        ];

        $mockedLogger = $this->createMock(WC_Logger::class);
        $mockedLogger->expects($this->once())
            ->method('log');

        $handler = new WooCommerceHandler($mockedLogger);
        $handler->handle($logRecord);
    }
}

