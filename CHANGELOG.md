## [1.13.2] - 2024-08-13
### Fixed
- backward compatibility, when second parameter in `SimpleLoggerFactory` constructor is null

## [1.13.1] - 2024-08-13
### Fixed
- removed redundant semicolon in `SimpleLoggerFactory` class

## [1.13.0] - 2024-08-09
### Changed
- Deprecated `WPDeskLoggerFactory`.
- Deprecated `Settings` class in `SimpleLoggerFactory`. Prefer using options array.
- Always use WooCommerce logger handler by default, and fallback to WordPress debug.log only, when previous is unavailable.
### Added
- `SimpleLoggerFactory` is now processing messages according to PSR-3 templates.
- From now on `SimpleLoggerFactory` adds unique ID for each session in context to ease identify messages in high-concurrency environments.
- Added `action_level` option to `SimpleLoggerFactory`, which uses `FingersCrossedHandler` underneath. You can use this option, to ensure, that messages after particular level are always logged (e.g. critical).

## [1.12.1] - 2024-03-26
### Fixed
- WC logger initialisation

## [1.12.0] - 2024-03-19
### Added
- sensitive data replacer

## [1.11.0] - 2023-07-10
### Updated
- monolog to 2.9.1

## [1.10.2] - 2022-11-15
### Fixed
- Deprecated error in PHP 8.X

## [1.10.1] - 2022-10-01
### Fixed
- WC logger initialisation

## [1.10.0] - 2022-08-16
### Added
- en_CA, en_GB translators

## [1.9.0] - 2022-08-16
### Added
- de_DE

## [1.8.0] - 2022-08-09
### Added
- en_AU, es_ES translators

## [1.7.4] - 2022-07-14
### Added
- Polish translations.

## [1.7.3] - 2022-05-19
### Fixed
- Fixed syntax error in file.

## [1.7.2] - 2022-05-18
### Fixed
- Fixed no handler actually assigned when using combination of NullHandler and actual one
- WooCommerce channel is now taken from logger registered channel

## [1.7.1] - 2022-04-15
### Fixed
- Fixed the getLogger method from SimpleLoggerFactory

## [1.7.0] - 2022-04-08
### Added
- Added `SimpleLoggerFactory` for basic logger use cases.

### Changed
- Minimum PHP required is now >=7.0

## [1.6.2] - 2020-07-21
### Fixed
- Notice error when cannot create log file

## [1.6.1] - 2020-05-25
### Fixed
- WooCommerceCapture checks if WC exists before proceeding

## [1.6.0] - 2019-05-21
### Added
- wpdesk_is_wp_log_capture_permitted filter to disable log capture

## [1.5.4] - 2019-05-06
### Fixed
- Exception: must be an instance of WC_Logger, instance of WPDesk\Logger\WC\WooCommerceMonologPlugin given

## [1.5.3] - 2019-04-25
### Fixed
- Exception while trying to disable log

## [1.5.2] - 2019-04-25
### Changed
- wp-notice 3.x

## [1.5.1] - 2019-04-23
### Changed
- Two files for custom loggers (default+custom)
### Fixed
- Tests

## [1.5.0] - 2019-04-18
### Changed
- Log file is unified with old way logger and all is logged in /wp-content/uploads/wpdesk-logs/wpdesk_debug.log
- Old static logger methods are deprecated
### Added
- All old way loggers are in deprecated dir and should work for old plugins
- Support for $shouldLoggerBeActivated static flag in factory - can return null logger

## [1.4.0] - 2019-01-21
### Changed
- WC integration now considers broken WC_Logger implementation
- Does not capture WC logger in WC < 3.5

## [1.3.1] - 2018-10-30
### Changed
- setDisableLog changes to disableLog

## [1.2.0] - 2018-10-29
### Changed
- getWPDeskFileName renamed to getFileName
- isWPDeskLogWorking renamed to isLogWorking
### Added
- most methods have $name parameter for using specific logger

## [1.1.1] - 2018-10-29
### Fixed
- should not capture all error - only log them
- boolean filter fixed

## [1.1.0] - 2018-10-29
### Added
- can disable logs using one bool
- less static variables

## [1.0.0] - 2018-10-28
### Added
- first stable version
- 80% coverage in integration tests
