[![pipeline status](https://gitlab.com/wpdesk/wp-logs/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-logs/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-logs/badges/master/coverage.svg)](https://gitlab.com/wpdesk/wp-logs/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-logs/v/stable)](https://packagist.org/packages/wpdesk/wp-logs) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-logs/downloads)](https://packagist.org/packages/wpdesk/wp-logs) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-logs/v/unstable)](https://packagist.org/packages/wpdesk/wp-logs) 
[![License](https://poser.pugx.org/wpdesk/wp-logs/license)](https://packagist.org/packages/wpdesk/wp-logs)

WordPress Library to log all that can be logged in a modern way.
===================================================

wp-log is a simple library for WordPress plugins which integrates Monolog with PHP/WP error handling and WooCommerce log system.

By default library logs to WC_Logger and WP Desk log file located in /uploads/wpdesk-logs/wpdesk_debug.log.

## Requirements

PHP 5.6 or later.

## Composer

You can install the bindings via [Composer](http://getcomposer.org/). Run the following command:

```bash
composer require wpdesk/wp-logs
```

To use the bindings, use Composer's [autoload](https://getcomposer.org/doc/01-basic-usage.md#autoloading):

```php
require_once 'vendor/autoload.php';
```

## Compatiblity between plugins

To ensure that always the latest and valid version of composer libraries are loaded in WP env you should use a solution
that ensure support between plugins and at least decreases the change that something would break. At the moment we recommend
using wpdesk/wp-autoloader.


## Manual instalation

If you do not wish to use Composer and wpdesk/wp-autoloader, you probably should stop using any existing library as it breaks compatibility between plugins.

## Getting Started

Simple usage to get Monolog style logger:

```php
WPDesk\Logger\LoggerFacade::get_logger()->addDebug('some debug message');

// Is equivalent to:
WPDesk\Logger\LoggerFacade::get_logger()->debug('some debug message');

// Is equivalent to:
WPDesk\Logger\LoggerFacade::get_logger()->log(Logger::DEBUG, 'some debug message');
```

You can also use various Facade helpers:
```php

// log WP_Errors
WPDesk\Logger\LoggerFacade::log_wp_error(new WP_Error(), debug_backtrace());

// log Exceptions
WPDesk\Logger\LoggerFacade::log_exception(new \RuntimeException());

// log message with backtrace
WPDesk\Logger\LoggerFacade::log_message_backtrace('some message', debug_backtrace(), Logger::DEBUG);

// log standard message
WPDesk\Logger\LoggerFacade::log_message('some message', ['some_user' => $user], 'my-app-source', Logger::DEBUG);


```


Logs can be used whether WC is active or not but if add_action is unavailable then WC logs capture would break.

## Project documentation

PHPDoc: https://wpdesk.gitlab.io/wp-logs/index.html  